{ mkDerivation
, stdenv
, base
, fuzzy
, optparse-applicative
, template-haskell
, datafile
}:
mkDerivation {
  pname = "emocli";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  enableSharedExecutables = false;
  preBuild = "export EMOCLI_DATAFILE=${datafile}";
  executableHaskellDepends = [
    base
    fuzzy
    optparse-applicative
    template-haskell
  ];
  license = stdenv.lib.licenses.gpl3;
}
