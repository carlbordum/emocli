{-# LANGUAGE DeriveLift #-}
module Types (Emoji (..), Config (..)) where


import Language.Haskell.TH.Syntax (Lift)


data Config = Config { results :: Int
                     , quiet :: Bool
                     , exact :: Bool
                     , searchTerm :: String
                     } deriving (Show)


data Emoji = Emoji { emoji   :: String
                   , name    :: String
                   , codes   :: [String]
                   , version :: String
                   } deriving (Lift, Show)
